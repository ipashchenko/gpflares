import numpy as np
import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
from inference import optimize, integrate_out
from flares_detection import find_flares, calculate_scb, suggest_flares
# import os
# Compile TF with SSE4.1 SSE4.2 AVX to speed up
# os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


def process(t, y, yerr, use_mcmc=True, p0=None, t_new=None, n_new=None,
            n_new_seed=None, dt_seed_span=10.0,
            n_samples=100, show_fig=False, file_fig=None, color="#1f77b4",
            fig=None, label=None, n_burn_in=100, n_prod=100, chain_file=None,
            n_walkers=24, threads=1, thin=1, use_hodlr=False, alpha_scb=None,
            grad_percentiles=None, n_std_grad=1.0):
    """
    :param t:
        Array of times.
    :param y:
        Array of the observed values.
    :param yerr:
        Array of the observed errors.
    :param use_mcmc:
        Boolean. Use MCMC to sample posterior (use Full Bayes)? If ``False``
        then just optimize HP and sample with this HP.
    :param p0: (optional)
        Initial values for the hyperparameters ("Amp", "log_alpha", "tau",
        "wn_disp").
    :param t_new: (optional)
        Array of times where to sample fitted GP. This allows more efficiently
        span time intervals near flare maximum. If ``None`` then time points
        where to sample fitted GP must be specified by ``n_new`` argument.
        (default: ``None``)
    :param n_new: (optional)
        Number of new points where to sample fitted GP. There will be ``n_new``
        points evenly distributed in ``(t[0], t[-1])`` interval. If ``None``
        then time points where to sample fitted GP must be specified in
        ``t_new`` argument. (default: ``None``)
    :param n_new_seed: (optional)
        Number of uniformly distributed grids to make preliminary estimate of
        the flare candidates positions to make new more dense non-uniform grid.
        If ``None`` than don't do preliminary flare search.
        (default: ``None``)
    :param dt_seed_span: (optional)
        Time interval [days] to span in new dense non-uniform grid buil using
        ``n_new_seed`` argument. (default: ``10``)
    :param n_samples: (optional)
        Number of samples to draw from predictive conditional distribution on
        time points specified by ``n_new`` or ``t_new`` arguments. (default: ``100``)
    :param show_fig: (optional)
        Show figure? (default: ``False``)
    :param file_fig: (optional)
        File to save picture of fitted GP. If ``None`` then don't save picture.
        (default: ``None``)
    :param color: (optional)
        Color for plots. If ``None`` then use standard. (default: ``None``)
    :param fig: (optional)
        Figure to plot. If ``None`` then create one. (default: ``None``)
    :param label: (optional)
        Label for the plot. If ``None`` then don't label. (default: ``None``)
    :param n_burn_in: (optional)
        Burnin size for MCMC. (default: ``100``)
    :param n_prod: (optional)
        Production size for MCMC. (default: ```100``)
    :param chain_file: (optional)
        File to store production chains in MCMC. If ``None`` than don't store.
        (default: ``None``)
    :param n_walkers: (optional)
        Number of walkers in ``emcee``. (default: ``24``)
    :param thin: (optional)
        Thin samples before selecting randomly ``n_samples`` in MCMC.
        (default: ``1``)
    :param threads: (optional)
        Number of threads when doing MCMC. (default: ``1``)
    :param use_hodlr: (optional)
        Use HODLR solver in GP?  (default: ``False``)
    :param alpha_scb: (optional)
        Significance level of a simultaneous confidence band. If ``None`` then
        don't build SCB (default:``None``)
    :param grad_percentiles: (optional)
        A iterable of two percentiles that used in calculating band around
        gradients. ``[16, 84]`` is equivalent to ``n_std_grad = 1``. If ``None``
        than don't use percentiles. (default: ``None``)
    :param n_std_grad: (optional)
        Number of STD to use in calculating band around gradients. (default:
        ``1``)
    :return:
    """

    if t_new is None:
        if n_new is None:
            if n_new_seed is None:
                raise Exception("Need one of t_new, n_new, n_new_seed"
                                " arguments!")
            t_new = np.linspace(t[0], t[-1], n_new_seed)
            # Just calculate mean with optimized HP
            result = optimize(t, y, yerr, p0=p0, t_new=t_new,
                              draw_samples=False)
            t_new = suggest_flares(t, result["mu"], dt_seed_span)
        else:
            t_new = np.linspace(t[0], t[-1], n_new)

    if use_mcmc:
        result = integrate_out(t, y, yerr, p0=p0, t_new=t_new,
                               n_samples=n_samples, n_burn_in=n_burn_in,
                               n_prod=n_prod, chain_file=chain_file,
                               n_walkers=n_walkers, threads=threads, thin=thin,
                               use_hodlr=use_hodlr)
    else:
        result = optimize(t, y, yerr, p0=p0, t_new=t_new, n_samples=n_samples)

    # Mean of predictive conditional for optimized HP (empirical Bayes) or MAP
    # (full Bayes).
    mu = result["mu"]
    # Samples from predictive conditional for optimized HP (empirical Bayes) o
    # samples from HP from posterior (full Bayes)
    gp_samples = result["samples"]
    t_new = result["t_new"]

    # Create figure with observed data
    if fig is None:
        fig = plt.figure(figsize=(12, 5))
    ax = fig.gca()
    ax.errorbar(t, y, yerr=yerr, fmt=".k", ms=3, elinewidth=0.5, label=label)

    # Calculate and plot gradients for samples from GP and mean
    grad_samples = list()
    for gp_sample in gp_samples:
        grad_sample = np.gradient(gp_sample, t_new)
        grad_samples.append(grad_sample)
        ax.plot(t_new, gp_sample, color=color, alpha=0.1)
        ax.plot(t_new, grad_sample, color="#ff7f0e", alpha=0.1)
    mu_grad = np.gradient(mu, t_new)
    ax.plot(t_new, mu_grad, color="k", lw=1)

    grad_samples = np.atleast_2d(grad_samples)

    # Built band around gradients
    std_grad = np.std(grad_samples, axis=0)
    if alpha_scb is not None:
        low_scb, up_scb = calculate_scb(grad_samples, mu_grad, 0.5*std_grad,
                                        alpha=alpha_scb)
    elif grad_percentiles is not None:
        low_scb, up_scb = np.percentile(grad_samples, grad_percentiles, axis=1)
    else:
        low_scb = mu_grad-n_std_grad*std_grad
        up_scb = mu_grad+n_std_grad*std_grad

    flares_params = find_flares(t_new, mu, gp_samples, n_std_grad=n_std_grad)

    for gp_sample, grad_sample in zip(gp_samples, grad_samples):
        ax.plot(t_new, gp_sample, color="#1f77b4", alpha=0.1, lw=0.5)
        ax.plot(t_new, grad_sample, color="#ff7f0e", alpha=0.1, lw=0.5)

    for flare_params in flares_params:
        t_max = flare_params["t_max"]
        if t_max is not None:
            ax.axvline(flare_params["t_start"], color="g", alpha=0.25)
            ax.axvline(flare_params["t_max"], color="r", alpha=0.5)
            ax.axvline(flare_params["t_end"], color="g", alpha=0.25)
            ax.axvspan(flare_params["t_start"], flare_params["t_end"],
                       color='g', alpha=0.1)
        else:
            ax.axvspan(flare_params["t_start"], flare_params["t_end"],
                       color='#ff7f0e', alpha=0.1)

    ax.set_xlabel("Time, MJD")
    ax.set_ylabel("Flux, Jy")
    ax.axhline(0, color='k', lw=0.5, alpha=0.5)
    ax.fill_between(t_new, low_scb, up_scb, color="#1f77b4", alpha=0.2)
    if use_mcmc:
        title = "Full Bayes"
    else:
        title = "Empirical Bayes"
    ax.set_title(title)
    if label is not None:
        fig.legend(loc="best")
    if show_fig:
        fig.show()
    if file_fig is not None:
        fig.savefig(file_fig)
    plt.gca().yaxis.set_major_locator(plt.MaxNLocator(5))

    return {"flares": flares_params, "inference_out": result, "fig": fig}


if __name__ == "__main__":
    import pickle
    with open("./data/0716+714_umrao_lc.pkl", "rb") as fo:
        data = pickle.load(fo)
    data15 = data[0]
    t = data15[:, 0]
    y = data15[:, 1]
    yerr = data15[:, 2]

    # result = process(t, y, yerr, use_mcmc=False, n_new=5000, n_std_grad=2.0)
    result = process(t, y, yerr, use_mcmc=True, n_new=5000, threads=4,
                     use_hodlr=True, n_burn_in=50, n_prod=50)
