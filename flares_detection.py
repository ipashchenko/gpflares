import numpy as np


def is_embraced(curve, means, widths):
    """
    Function that checks if 1D curve is embraced by 1D band.

    :param curve:
        Iterable of curve's points.
    :param means:
        Iterable of band's means.
    :param widths:
        Iterable of widths (sigmas).
    :return:
        ``True`` if curve is embraced by band.
    """
    curve = np.array(curve)
    means = np.array(means)
    widths = np.array(widths)
    assert len(curve) == len(means) == len(widths)
    diff = np.abs(curve - means)

    return np.alltrue(np.nan_to_num(diff) <= np.nan_to_num(widths))


def count_contained(curves, means, widths):
    """
    Count haw many curves are contained inside given band.

    :param curves:
        Iterable of numpy 1D arrays with curves to count.
    :param means:
        Iterable of band's means.
    :param widths:
        Iterable of widths (sigmas).
    :return:
        Number of curves within band.
    """
    i = 0
    for curve in curves:
        if is_embraced(curve, means, widths):
            i += 1
    return i


def calculate_scb(curves, mu, std, alpha=0.05, delta=0.01):
    """
    Calculate simulatanious confidence band.

    :param curves:
        Samples of GP.
    :param mu:
        Mean values.
    :param std:
        STD values.
    :param alpha: (optional)
        Significance level of a corresponding hypothesis test. (default:
        ``0.05``)
    :param delta: (optional)
        Step of widening of band. (default: ``0.01``)
    :return:
        Two 1D numpy arrays with low and upper alpha-level simultaneous
        confidence bands.
    """
    n = count_contained(curves, mu, std)
    f = 1
    while n < len(curves) * alpha:
        f += delta
        n = count_contained(curves, mu, f*std)
    return mu - f*std, mu + f*std


# FIXME: Implement recursive overlappings
def suggest_flares(t, mu, dt_span):
    """
    Function that suggests location of flares.

    :param t:
        Time values.
    :param mu:
        Array of mean values.
    :param dt_span:
        Duration of candidate flare.
    :return:
    """
    grad = np.gradient(mu, t)
    dgrad = np.gradient(grad, t)
    grad_sign_changes = np.diff(np.sign(grad)) != 0
    idxs_grad_sign_changes = np.where(grad_sign_changes)[0]
    max_indxs = list()
    for ind in idxs_grad_sign_changes:
        if dgrad[ind] < 0:
            max_indxs.append(ind)

    max_ts = t[np.array(max_indxs)]

    time_grid = list()
    for max_t in max_ts:
        trange = [max_t-dt_span, max_t+dt_span]
        time_grid.append(trange)

    overlappings = list()
    for x, y in zip(time_grid[:-1], time_grid[1:]):
        if x[-1]>=y[0]:
            overlappings.append([x, y])


def find_flares(t, mu, samples, alpha=None, dt_min=None, n_std_grad=1.0):
    """
    Find parameters of flares using samples from GP.

    :param t:
        Time values.
    :param mu:
        Array of mean values.
    :param samples:
        Iterable of arrays. Conditional samples from GP (with fixed HP in
        empirical Bayes and HP from their posterior in full Bayes).
    :param alpha: (optional)
        Significance level of a corresponding hypothesis test. If ``None`` then
        don't build SCB - just use band of pointwise STD. (default:``None``)
    :param dt_min: (optional)
        Maximum duration of a flare [d].
    :return:
        List of (start_indx, max_indx, end_indx) of flare parameters. If there
        are many maximums inside start_indx and end_indx, then max_indx is None.
    """
    grad_samples = list()
    dgrad_samples = list()
    for sample in samples:
        grad_sample = np.gradient(sample, t)
        dgrad_sample = np.gradient(grad_sample, t)
        grad_samples.append(grad_sample)
        dgrad_samples.append(grad_sample)
    grad_samples = np.atleast_2d(grad_samples)
    dgrad_samples = np.atleast_2d(dgrad_samples)
    mu_grad = np.gradient(mu, t)
    mu_dgrad = np.gradient(mu_grad, t)
    std_grad = np.std(grad_samples, axis=0)
    std_dgrad = np.std(dgrad_samples, axis=0)
    # Low and up boundaries of SCB for gradients
    if alpha is None:
        low = mu_grad-std_grad * n_std_grad
        up = mu_grad+std_grad * n_std_grad
        dlow = mu_dgrad-std_dgrad
        dup = mu_dgrad-std_dgrad
    else:
        low, up = calculate_scb(grad_samples, mu_grad, std_grad, alpha=alpha)
        dlow, dup = calculate_scb(dgrad_samples, mu_dgrad, std_dgrad, alpha=alpha)

    # Find first intersection of low or upper SCB bounds with gradient==0 line.
    low_sign_changes = np.array((np.diff(np.sign(low)) != 0), dtype=int)
    up_sign_changes = np.array((np.diff(np.sign(up)) != 0), dtype=int)
    # The same for mean gradient
    mu_grad_sign_changes = np.array((np.diff(np.sign(mu_grad)) != 0), dtype=int)

    # There's 2 conditions on maxima:
    # 1) First low SCB of gradients crosses gradient==0 line. Then upper (not
    # that after low crossing low bound crossing it again)
    # 2) Mean gradient crosses gradient==0 line between low and upper bounds of
    # SCB

    crossings_mu = np.where(mu_grad_sign_changes > 0)[0]
    crossings_low = np.where(low_sign_changes > 0)[0]
    crossings_up = np.where(up_sign_changes > 0)[0]

    idxs = np.unique(crossings_low[np.searchsorted(crossings_low, crossings_up)-1])

    flares = list()
    for idx_low in idxs:
        # Find next lowest element in ``crossings_up`` that is higher than
        # ``idx``
        try:
            delta = list(crossings_up-idx_low)
            idx_high = np.min([i for i in delta if i>0])
            idx_high = delta.index(idx_high)
            idx_high = crossings_up[idx_high]

            if dt_min is not None:
                if idx_high-idx_low>dt_min:
                    continue

            # Only check that mean gradient crossing are with the bounds:
            max = np.intersect1d(np.arange(idx_low, idx_high+1), crossings_mu)
            if not max.size:
                continue
            elif max.size > 1:
                t_max = None
            else:
                t_max = t[max[0]]
            flares.append({"t_start": t[idx_low],
                           "t_max": t_max,
                           "t_end": t[idx_high],
                           "y_t_max": mu[max[0]],
                           "std_y_t_max": np.std([sample[max[0] for sample in samples]])})
        except ValueError:
            print "Value error at: ", t[idx_low]
            pass

    return flares

