# gpflares

Gaussian Process Flare Finding Tools

### Timings on my laptop (i3)

Light curve with 1000 observed points.

* Sample conditional in MCMC regime using TensorFlow:
    * 5000 new time values - 49s/it
    * 3000 new time values - 8 s/it

* One MCMC step with 5000 points and 24 walkers and 4 threads
    * HODLR (tol=10**(-11) - seems to be no problems with resulting covariances)
    - 1.5-2 s
    * Basic solver (no problems with covariances) - ??? s


### Timings on FRB workstation (i5)

* Sample conditional in MCMC regime using TensorFlow:

    * 5000 new time values - **9s/it**

* One MCMC step with 5000 points and 24 walkers and 4 threads
    * HODLR (tol=10**(-11) - seems to be no problems with resulting covariances)
    - 30 s
    * Basic solver (no problems with covariances) - **1.75 s**
    
Thus doing MCMC with burning=250, 500 production steps with 24 walkers and 10x
thinning (this results in 1200 samples from HP posterior) costs 20 minutes. And
sampling 100 draws ~ 15 minutes.


