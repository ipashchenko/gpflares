import numpy as np
import tensorflow as tf
from tensorflow.contrib.distributions import MultivariateNormalFullCovariance
from tqdm import tqdm


def nll(p, y, gp):
    """
    :param p:
        Mean, log(Disp_WN), log(A**2), log(alpha), log(scale**2)
    """
    gp.set_parameter_vector(p)
    ll = gp.log_likelihood(y, quiet=True)
    return -ll if np.isfinite(ll) else 1e25


def grad_nll(p, y, gp):
    """
    :param p:
        Mean, log(Disp_WN), log(A**2), log(alpha), log(scale**2)
    """
    gp.set_parameter_vector(p)
    return -gp.grad_log_likelihood(y, quiet=True)


def log_probability(params, gp, t, y, yerr):
    gp.set_parameter_vector(params)
    gp.compute(t, yerr)
    lp = gp.log_prior()
    if not np.isfinite(lp):
        return -np.inf
    return gp.log_likelihood(y)+lp


def sample_conditional(gp, t, t_new, y, yerr, hp_samples):
    """
    Sample from predictive conditional distribution for each HP in a number of
    specified HPs.

    :param gp:
        Instance of ``george.GP``.
    :param t:
        Array of train times (that corresponds to the observed values on which
        we will conditional our predictions).
    :param t_new:
        Array of new times.
    :param y:
        Array of the observed data.
    :param yerr:
        Array of the observed errors.
    :param hp_samples:
        Iterable of HP.
    :return:
        List of samples from GP.
    """
    gp_samples = list()

#    print("t = {}".format(t))
#    print("t_new = {}".format(t_new))

    inp1 = tf.placeholder(tf.float64, shape=(len(t_new)))
    inp2 = tf.placeholder(tf.float64, shape=(len(t_new), len(t_new)))
    gp_sample = MultivariateNormalFullCovariance(loc=inp1,
                                                 covariance_matrix=inp2).sample()
    eye = np.eye(len(t_new))
    for s in tqdm(hp_samples):
        gp.set_parameter_vector(s)
        gp.compute(t, yerr)
        mu, cov = gp.predict(y, t_new)
        cov += 0.001 * eye
        with tf.Session() as sess:
            gp_samples.append(sess.run([gp_sample], feed_dict={inp1: mu,
                                                               inp2: cov})[0])
    return gp_samples


def print_acceptance_fraction(acceptance_fraction):
    """
    Debug function that prints acceptance fraction statistics.
    """
    min_acc = np.min(acceptance_fraction)
    median_acc = np.median(acceptance_fraction)
    mean_acc = np.mean(acceptance_fraction)
    max_acc = np.max(acceptance_fraction)
    print("Acceptance fraction (min, median, mean, max) :"
          " {} {} {} {}".format(min_acc, median_acc, mean_acc, max_acc))
