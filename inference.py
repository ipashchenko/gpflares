import numpy as np
import george
import emcee
from tqdm import tqdm
from functools import partial
from utils import (nll, grad_nll, log_probability, print_acceptance_fraction,
                   sample_conditional)
import scipy.optimize as op
from sklearn.preprocessing import MinMaxScaler


def optimize(t, y, yerr, p0=None, t_new=None, draw_samples=True, n_samples=100):
    """
    Optimize GP hyperparameters for given data.
    :param t:
        Array of times.
    :param y:
        Array of the observed values.
    :param yerr:
        Array of the observed errors.
    :param p0: (optional)
        Initial values for the hyperparameters ("Amp", "log_alpha", "tau",
        "wn_disp").
    :param t_new: (optional)
        Array of times where to sample fitted GP. This allows more efficiently
        span time intervals near flare maximum. If ``None`` then time points
        where to sample fitted GP must be specified by ``n_new`` argument.
        (default: ``None``)

    :return:
        Dictionary with results.
    :notes:
        Points where to sample fitted GP could be specified with both ``t_new``
        and ``n_new`` but former has the priority. If none is specified than
        predictions for those points in ``t`` are returned.
    """
    if p0 is None:
        p0 = [np.mean(y), 0.2, 1, -2, 200]

    kernel = p0[2]**2*george.kernels.RationalQuadraticKernel(log_alpha=p0[3],
                                                             metric=p0[4]**2)
    gp = george.GP(kernel, mean=np.mean(y), fit_mean=True,
                   white_noise=np.log(p0[1]**2), fit_white_noise=True)
    nll_ = partial(nll, y=y, gp=gp)
    grad_nll_ = partial(grad_nll, y=y, gp=gp)

    gp.compute(t, yerr)
    print("Initial log of marginalized likelihood : {}".format(gp.log_likelihood(y)))
    p0 = gp.get_parameter_vector()
    print("Optimizing HP...")
    results = op.minimize(nll_, p0, jac=grad_nll_, method="L-BFGS-B")

    gp.set_parameter_vector(results.x)
    gp.compute(t, yerr)
    print("Final log of marginalized likelihood   : {}".format(gp.log_likelihood(y)))
    print("Optimized HP : {}".format(results.x))

    # Kernel means using only RQ kernel in predictions
    mu = gp.predict(y, t_new, return_cov=False, kernel=kernel)

    if draw_samples:
        print("Sampling from predictive conditional distribution using"
              " optimized HP...")
        gp_samples = gp.sample_conditional(y, t_new, size=n_samples)
    else:
        gp_samples = None

    return {"gp": gp, "logL": gp.log_likelihood(y), "mu": mu, "t_new": t_new,
            "t": t, "y": y, "yerr": yerr, "samples": gp_samples}


def integrate_out(t, y, yerr, p0=None, t_new=None, n_samples=100, n_burn_in=100,
                  n_prod=100, chain_file=None, n_walkers=24, threads=1, thin=1,
                  use_hodlr=False):
    """
    MCMC GP hyperparameters for given data.

    :param t:
        Array of times.
    :param y:
        Array of the observed values.
    :param yerr:
        Array of the observed errors.
    :param p0: (optional)
        Initial values for the hyperparameters ("Amp", "log_alpha", "tau",
        "wn_disp").
    :param t_new: (optional)
        Array of times where to sample fitted GP. This allows more efficiently
        span time intervals near flare maximum. If ``None`` then time points
        where to sample fitted GP must be specified by ``n_new`` argument.
        (default: ``None``)
    :param n_samples: (optional)
        Number of samples from GP posterior to realize on time points specified
        by ``n_new`` or ``t_new`` arguments. (default: ``100``)
    :param n_burn_in: (optional)
        Burnin size. (default: ``100``)
    :param n_prod: (optional)
        Production size. (default: ```100``)
    :param n_walkers: (optional)
        Number of walkers in ``emcee``. (default: ``24``)
    :param thin: (optional)
        Thin samples before selecting randomly ``n_samples``. (default: ``1``)
    :param use_hodlr: (optional)
        Use HODLR solver for GP? It's usage can occasionally results in problems
        with sampling from predictive conditional distribution, but gives nearly
        3x speedup with ``tol=10**(-11)``.

    :return:
        Dictionary with results.
    :notes:
        Points where to sample fitted GP could be specified with both ``t_new``
        and ``n_new`` but former has the priority. If none is specified than
        samples for those points in ``t`` are returned and plotted.
    """
    if chain_file is not None:
        fn = open(chain_file, "w")
        fn.close()

    # Transform first because using HODLR
    scaler = MinMaxScaler()
    t_ = scaler.fit_transform(t[:, None])[:, 0]
    t_new_ = scaler.transform(t_new[:, None])[:, 0]

    if p0 is None:
        p0 = [np.mean(y), 0.2, 1, -2, 200./scaler.data_range_]

    kernel = p0[2]**2*george.kernels.RationalQuadraticKernel(log_alpha=p0[3],
                                                             metric=p0[4]**2)
    # Using HODLR may result in not positive semidefinite covariance and
    # failling of tf with ``tf.errors.InvalidArgumentError``
    if use_hodlr:
        solver = george.HODLRSolver
        kwargs = {"tol": 10**(-11)}
    else:
        solver = None
        kwargs = {}

    gp = george.GP(kernel, mean=np.mean(y), fit_mean=True,
                   white_noise=np.log(p0[1]**2), fit_white_noise=True,
                   solver=solver, **kwargs)
    nll_ = partial(nll, y=y, gp=gp)
    grad_nll_ = partial(grad_nll, y=y, gp=gp)

    gp.compute(t_, yerr)
    print("Initial log of marginalized likelihood : {}".format(gp.log_likelihood(y)))
    p0 = gp.get_parameter_vector()
    print("Optimizing HP...")
    results = op.minimize(nll_, p0, jac=grad_nll_, method="L-BFGS-B")

    gp.set_parameter_vector(results.x)
    print("Final log of marginalized likelihood   : {}".format(gp.log_likelihood(y)))
    print("Optimized HP : {}".format(results.x))

    p0 = gp.get_parameter_vector()
    ndim = len(p0)
    sampler = emcee.EnsembleSampler(n_walkers, ndim, log_probability,
                                    args=(gp, t_, y, yerr), threads=threads)

    p0 = p0+1e-5*np.random.randn(n_walkers, ndim)
    print("Sampling HP posterior distribution...")
    print("Running burn-in...")
    for i, (p0, lp, _) in tqdm(enumerate(sampler.sample(p0, iterations=n_burn_in,
                                                        storechain=True)),
                               total=n_burn_in, dynamic_ncols=True):
        pass

        # This shows problems when HODLR ``tol`` is large
        # if not i % 10:
        #     print_acceptance_fraction(sampler.acceptance_fraction)

    p_map = sampler.flatchain[np.argmax(sampler.lnprobability)]
    print("Current MAP : {}".format(p_map))
    sampler.reset()

    print("Resetting and running production ...")
    for i, (p0, lp, _) in tqdm(enumerate(sampler.sample(p0, iterations=n_prod,
                                                        storechain=True)),
                               total=n_prod, dynamic_ncols=True):
        if chain_file is not None:
            f = open(chain_file, "a")
            for k in range(p0.shape[0]):
                f.write("{0:4d} {1:s}\n".format(k, " ".join(p0[k])))
            f.close()

        # This shows problems when HODLR ``tol`` is large
        # if not i % 10:
        #     print_acceptance_fraction(sampler.acceptance_fraction)

    # Using MAP value of HP to find mean
    p_map = sampler.flatchain[np.argmax(sampler.lnprobability)]
    gp.set_parameter_vector(p_map)
    gp.compute(t_, yerr)
    mu_map = gp.predict(y, t_new_, return_cov=False)

    hp_samples = sampler.flatchain[::thin]
    print("Sampling from predictive conditional distribution using HP from"
          " posterior...")

    hp_samples = hp_samples[np.random.randint(len(hp_samples), size=n_samples)]
    gp_samples = sample_conditional(gp, t_, t_new_, y, yerr, hp_samples)

    return {"sampler": sampler, "t_new": t_new, "t": t, "y": y,
            "yerr": yerr, "samples": gp_samples, "mu": mu_map, "gp": gp}

